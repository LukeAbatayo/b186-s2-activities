//Translate the other students from our boilerplate code into their own respective objects.

let studentOne = {
  name: 'John',
  email: 'john@mail.com',
  grades: [89, 84, 78, 88],
  login() {
    console.log(`${this.email} has logged in`);
  },
  logout() {
    console.log(`${this.email} has logged out`);
  },
  listGrades() {
    console.log(`${this.name}'s quarterly averages are: ${this.grades}`);
  },
};

let studentTwo = {
  name: 'Joe',
  email: 'joe@mail.com',
  grades: [78, 82, 79, 85],
  login() {
    console.log(`${this.email} has logged in`);
  },
  logout() {
    console.log(`${this.email} has logged out`);
  },
  listGrades() {
    console.log(`${this.name}'s quarterly averages are: ${this.grades}`);
  },
};

let studentThree = {
  name: 'Jane',
  email: 'jane@mail.com',
  grades: [87, 89, 91, 93],
  login() {
    console.log(`${this.email} has logged in`);
  },
  logout() {
    console.log(`${this.email} has logged out`);
  },
  listGrades() {
    console.log(`${this.name}'s quarterly averages are: ${this.grades}`);
  },
};

let studentFour = {
  name: 'Jessie',
  email: 'jessie@mail.com',
  grades: [91, 89, 92, 93],
  login() {
    console.log(`${this.email} has logged in`);
  },
  logout() {
    console.log(`${this.email} has logged out`);
  },
  listGrades() {
    console.log(`${this.name}'s quarterly averages are: ${this.grades}`);
  },
};

//Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)

studentOne.computeAve = function () {
  const average = this.grades.reduce((a, b) => a + b, 0) / this.grades.length;

  console.log(`${this.name}'s quarterly averages grades: ${average}`);
};

studentTwo.computeAve = function () {
  const average = this.grades.reduce((a, b) => a + b, 0) / this.grades.length;

  console.log(`${this.name}'s quarterly averages grades: ${average}`);
};

studentThree.computeAve = function () {
  const average = this.grades.reduce((a, b) => a + b, 0) / this.grades.length;

  console.log(`${this.name}'s quarterly averages grades: ${average}`);
};

studentFour.computeAve = function () {
  const average = this.grades.reduce((a, b) => a + b, 0) / this.grades.length;

  console.log(`${this.name}'s quarterly averages grades: ${average}`);
};

//Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.

studentOne.willPass = function () {
  const average = this.grades.reduce((a, b) => a + b, 0) / this.grades.length;

  if (average >= 85) {
    return true;
  } else {
    return false;
  }
};

studentTwo.willPass = function () {
  const average = this.grades.reduce((a, b) => a + b, 0) / this.grades.length;

  if (average >= 85) {
    return true;
  } else {
    return false;
  }
};

studentThree.willPass = function () {
  const average = this.grades.reduce((a, b) => a + b, 0) / this.grades.length;

  if (average >= 85) {
    return true;
  } else {
    return false;
  }
};

studentFour.willPass = function () {
  const average = this.grades.reduce((a, b) => a + b, 0) / this.grades.length;

  if (average >= 85) {
    return true;
  } else {
    return false;
  }
};

//Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

studentOne.willPassWithHonors = function () {
  const average = this.grades.reduce((a, b) => a + b, 0) / this.grades.length;

  if (average >= 90) {
    return true;
  } else if (average >= 85 && average < 90) {
    return false;
  } else {
    return undefined;
  }
};

studentTwo.willPassWithHonors = function () {
  const average = this.grades.reduce((a, b) => a + b, 0) / this.grades.length;

  if (average >= 90) {
    return true;
  } else if (average >= 85 && average < 90) {
    return false;
  } else {
    return undefined;
  }
};

studentThree.willPassWithHonors = function () {
  const average = this.grades.reduce((a, b) => a + b, 0) / this.grades.length;

  if (average >= 90) {
    return true;
  } else if (average >= 85 && average < 90) {
    return false;
  } else {
    return undefined;
  }
};

studentFour.willPassWithHonors = function () {
  const average = this.grades.reduce((a, b) => a + b, 0) / this.grades.length;

  if (average >= 90) {
    return true;
  } else if (average >= 85 && average < 90) {
    return false;
  } else {
    return undefined;
  }
};

//Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.

classOf1A = {
  students: [studentOne, studentTwo, studentThree, studentFour],
};

//Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.

classOf1A.countHonorStudents = function () {
  let honorStudents = classOf1A.students.filter(student => student.grades.reduce((a, b) => a + b, 0) / student.grades.length >= 90);

  return honorStudents.length;
};

//Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

classOf1A.honorsPercentage = function () {
  return (100 * classOf1A.countHonorStudents()) / classOf1A.students.length;
};

//Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.

classOf1A.retrieveHonorStudentInfo = function () {
  classOf1A.students.filter();
};

//Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.
